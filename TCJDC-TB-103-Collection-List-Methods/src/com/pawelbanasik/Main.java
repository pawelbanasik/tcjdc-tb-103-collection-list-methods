package com.pawelbanasik;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Theatre theatre = new Theatre("Olympian", 8, 12);

		// shallow copy - sposob 1
		// seatCopy to lista referencji to tych samych obiektów listy siedzen
		List<Theatre.Seat> seatCopy = new ArrayList<>(theatre.seats);
		printList(seatCopy);

		seatCopy.get(1).reserve();
		if (theatre.reserveSeat("A02")) {
			System.out.println("Please pay for A02");
		} else {
			System.out.println("Seat already reserved");
		}

		// metoda do tasowania randomowo elementów listy
		Collections.shuffle(seatCopy);
		System.out.println("Printing seatCopy");
		printList(seatCopy);
		System.out.println("Printing theatre.seat");
		printList(theatre.seats);

		// maksymalny i minimalny element listy
		Theatre.Seat minSeat = Collections.min(seatCopy);
		Theatre.Seat maxSeat = Collections.max(seatCopy);
		System.out.println("Min seat number is " + minSeat.getSeatNumber());
		System.out.println("Max seat number is " + maxSeat.getSeatNumber());

		// metoda do sortowania ktora sam napisal (nie jest wbudowana w Jave!!!)
		sortList(seatCopy);
		System.out.println("Printing sorted seatCopy");
		printList(seatCopy);

		// shallow copy - sposob 2
		// znow shallow copy ale nie bedzie dzialac bo nowa lista musi miec elementy w
		// sobie
		// dlatego powiedzial ze trudno dla tej dziwnej metody znalezc
		// zastosowanie
		// List<Theatre.Seat> newList = new ArrayList<>(theatre.seats.size());
		// Collections.copy(newList, theatre.seats);
	}

	public static void printList(List<Theatre.Seat> list) {
		for (Theatre.Seat seat : list) {
			System.out.print(" " + seat.getSeatNumber());
		}
		System.out.println();
		System.out.println("======================================================================");
	}

	// generyczna metoda na sortowanie z wykorzystaniem swap() czyli zamiany elementów w liscie
	// ? oznacza wildcard kazda klase ktora dziedziczy po Theatre.Seat lub jest jej podklasa
	public static void sortList(List<? extends Theatre.Seat> list) {
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).compareTo(list.get(j)) > 0) {
					Collections.swap(list, i, j);
				}
			}
		}
	}

}
